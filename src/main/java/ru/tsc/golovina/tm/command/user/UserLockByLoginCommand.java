package ru.tsc.golovina.tm.command.user;

import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class UserLockByLoginCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getCommand() {
        return "user-lock-by-login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        final String id = serviceLocator.getUserService().findUserByLogin(login).getId();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

}
