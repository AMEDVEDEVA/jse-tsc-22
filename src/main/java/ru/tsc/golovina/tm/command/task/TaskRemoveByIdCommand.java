package ru.tsc.golovina.tm.command.task;

import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public String getCommand() {
        return "task-remove-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getTaskService().remove(userId, task);
    }

}
