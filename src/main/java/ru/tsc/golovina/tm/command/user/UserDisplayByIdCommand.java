package ru.tsc.golovina.tm.command.user;

import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserDisplayByIdCommand extends AbstractUserCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getCommand() {
        return "user-display-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display user by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        showUser(user);
    }

}
