package ru.tsc.golovina.tm.command.user;

import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.empty.EmptyUserListException;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.model.User;

import java.util.List;

public final class UserListCommand extends AbstractUserCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getCommand() {
        return "user-list";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list of users";
    }

    @Override
    public void execute() {
        System.out.println("User list");
        final List<User> users = serviceLocator.getUserService().findAll();
        if (null == users) throw new EmptyUserListException();
        for (final User user : users)
            showUser(user);
    }

}
