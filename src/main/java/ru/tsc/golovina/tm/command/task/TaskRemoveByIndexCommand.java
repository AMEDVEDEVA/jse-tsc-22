package ru.tsc.golovina.tm.command.task;

import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public String getCommand() {
        return "task-remove-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getTaskService().remove(userId, task);
    }

}
