package ru.tsc.golovina.tm.command.system;

import ru.tsc.golovina.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "exit";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
