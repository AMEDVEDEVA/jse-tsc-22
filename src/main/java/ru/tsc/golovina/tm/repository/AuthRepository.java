package ru.tsc.golovina.tm.repository;

import ru.tsc.golovina.tm.api.repository.IAuthRepository;

public final class AuthRepository implements IAuthRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
