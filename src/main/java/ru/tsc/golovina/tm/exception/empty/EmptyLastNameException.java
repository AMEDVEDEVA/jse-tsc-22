package ru.tsc.golovina.tm.exception.empty;

import ru.tsc.golovina.tm.exception.AbstractException;

public class EmptyLastNameException extends AbstractException {

    public EmptyLastNameException() {
        super("Error. Empty last name");
    }

}
